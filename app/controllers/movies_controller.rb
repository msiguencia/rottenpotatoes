class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index
    p = {}
    if params[:sort].nil?
        p[:sort] = session[:sort]
    elsif session[:sort] != params[:sort]
        session[:sort] = params[:sort]
    end
    if params[:ratings].nil?
        p[:ratings] = session[:ratings]
    elsif session[:ratings] != params[:ratings]
        session[:ratings] = params[:ratings]
    end
    if !p[:sort].nil? || !p[:ratings].nil?
        if p[:sort].nil?
            p[:sort] = params[:sort]
        elsif p[:ratings].nil?
            p[:ratings] = params[:ratings]
        end
        flash.keep
        redirect_to movies_path p
    end
    @all_ratings = Movie.ratings
    @sort_by = params[:sort]
    filter_by = nil
    if !params[:ratings].nil?
        @ratings = params[:ratings]
        filter_by = {:rating => @ratings.keys}
    end
    @movies = Movie.find(:all, :order => @sort_by, :conditions => filter_by)
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
