class Movie < ActiveRecord::Base
  @@ratings = ['G', 'PG', 'PG-13', 'R']
  attr_accessible :title, :rating, :description, :release_date
  cattr_reader :ratings
end
